import java.io.IOException;
import java.util.ArrayList;
import java.util.*;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;

public class WineReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
   protected void reduce(Text token, Iterable<DoubleWritable> records, Context context) throws IOException, InterruptedException {

      int count = 0;
      double sum = 0.0;
      double average = 0.0;


      for (DoubleWritable record : records) {   // Aggregate the shuffled values
         count++;
         sum += record.get();
      }

      if (count > 0) {
         average = sum/count;
      }
      // Write key (token for carrier) and value as the total number of flights for the particular carrier
      context.write(token, new DoubleWritable(average));  // Context object: where output key/value pairs are written.

   }  // end of protected map()
} // end of class