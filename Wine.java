import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;

public class Wine {
   public static void main(String[] args) throws Exception {
      runJob(args,  "Fixed_Acidity", 0);
      runJob(args, "Volatile_Acidity", 1);
      runJob(args, "Citric_Acidity", 2);
      runJob(args, "Residual_Sugar", 3);
      runJob(args, "Chlorides", 4);
      runJob(args,  "Free_Sulfur_Dioxide", 5);
      runJob(args, "Total_Sulfur_Dioxide", 6);
      runJob(args,  "Density", 7);
      runJob(args, "PH", 8);
      runJob(args,  "Sulphates", 9);
      runJob(args, "Alcohol", 10);


   } // end of main

   public static void runJob(String[] args, String name, Integer col) throws Exception {
      Job job = new Job();
      job.setJarByClass(Wine.class);
      job.setJobName("WineTestJob");
      
      // Identifying HDFS path for the input data
      TextInputFormat.addInputPath(job, new Path(args[0]));
      job.setInputFormatClass(TextInputFormat.class);  //the expected format of the data, by default, input format is TextInputFormat
      
      //Define the overall structure of the MapReduce application
      switch (name) {
         case "Fixed_Acidity" : job.setMapperClass(FixedAcidityMapper.class);break;
         case "Volatile_Acidity" : job.setMapperClass(VolatileAcidityMapper.class);break;
         case "Citric_Acidity" : job.setMapperClass(CitricAcidityMapper.class);break;
         case "Residual_Sugar" : job.setMapperClass(ResidualSugarMapper.class);break;
         case "Chlorides" : job.setMapperClass(ChloridesMapper.class);break;
         case "Free_Sulfur_Dioxide" : job.setMapperClass(FreeSulfurDioxideMapper.class);break;
         case "Total_Sulfur_Dioxide" : job.setMapperClass(TotalSulfurDioxideMapper.class);break;
         case "Density" : job.setMapperClass(DensityMapper.class);break;
         case "PH" : job.setMapperClass(PHMapper.class);break;
         case "Sulphates" : job.setMapperClass(SulphatesMapper.class);break;
         case "Alcohol" : job.setMapperClass(AlcoholMapper.class);break;
         default: throw new Exception("UNKNOWN MAPPER");
      }
//      job.setMapperClass(WineMapper.class); // Specify both the Mapper and Reducer classes
      job.setReducerClass(WineReducer.class);
      
      // Indicate the HDFS path for the application's output as well as the format of the data
      TextOutputFormat.setOutputPath(job, new Path(args[1] + "/" + name));
      job.setOutputFormatClass(TextOutputFormat.class);
      job.setOutputKeyClass(Text.class);
      job.setOutputValueClass(DoubleWritable.class);
    
      // Run the job and wait
      job.waitForCompletion(true);  // The driver waits at this point unitl the waitForCompletion function returns
   }
  
} // end of class FlightsByCarrier